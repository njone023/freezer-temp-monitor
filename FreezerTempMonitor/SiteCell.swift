//
//  SiteCell.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/6/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import Foundation
import UIKit

class SiteCell: UITableViewCell {
    @IBOutlet weak var siteNameLabel: UILabel!
    @IBOutlet weak var warningLabel: UILabel!
}
