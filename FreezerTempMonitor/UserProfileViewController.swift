//
//  UserProfileViewController.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 11/21/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import UIKit
import Foundation

class UserProfileViewController: UIViewController {
    
    var apiManager = APIManager()
    
    //user info
    @IBOutlet weak var userRealNameLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userCompanyLabel: UILabel!
    @IBOutlet weak var languageControl: UISegmentedControl!
    @IBOutlet weak var tempControl: UISegmentedControl!
    
    //static labels
    @IBOutlet weak var staticUserRealNameLabel: UILabel!
    @IBOutlet weak var staticUserNameLabel: UILabel!
    @IBOutlet weak var staticUserCompanyLabel: UILabel!
    @IBOutlet weak var staticUserLanguageLabel: UILabel!
    @IBOutlet weak var staticUserTempLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userRealNameLabel.text = getParent().currentUser?.userName
        userNameLabel.text = getParent().currentUser?.userId
        userCompanyLabel.text = getParent().currentUser?.userCompany
        languageControl.selectedSegmentIndex = getParent().currentUser.userLanguage == Languages.english.rawValue ? 0 : 1
        tempControl.selectedSegmentIndex = getParent().currentUser.userTempUnit == TempUnits.celsius.rawValue ? 0 : 1
        updateLabels()
    }
    
    //change any labels to correct language depending on user's language setting
    @IBAction func languageChanged(_ sender: UISegmentedControl) {
        if getParent().isDatabaseUser {
            if let index = getParent().userDatabase.allUsers.index(of: getParent().userDatabase.currentUser) {
                switch languageControl.selectedSegmentIndex {
                case 0:
                    getParent().userDatabase.allUsers[index].userLanguage = Languages.english.rawValue
                case 1:
                    getParent().userDatabase.allUsers[index].userLanguage = Languages.spanish.rawValue
                default:
                    break;
                }
            }
            getParent().userDatabase.updateUser(userId: getParent().currentUser.userId!, userRole: "", userName: "", userCompany: "", userLanguage: getParent().currentUser.userLanguage!, userTempUnit: "")
        }
            //user in in API, just update current user for their logged in session
        else {
            switch languageControl.selectedSegmentIndex {
            case 0:
                getParent().userDatabase.currentUser.userLanguage = Languages.english.rawValue
            case 1:
                getParent().userDatabase.currentUser.userLanguage = Languages.spanish.rawValue
            default:
                break;
            }
        }
        updateLabels()
        getParent().checkLabels()
    }
    
    //if user changed the temp unit, make changes in UI, core data or API
    @IBAction func tempUnitChanged(_ sender: UISegmentedControl) {
        //if core data user
        if getParent().isDatabaseUser {
            if let index = getParent().userDatabase.allUsers.index(of: getParent().userDatabase.currentUser) {
                switch tempControl.selectedSegmentIndex {
                case 0:
                    getParent().userDatabase.allUsers[index].userTempUnit = TempUnits.celsius.rawValue
                case 1:
                    getParent().userDatabase.allUsers[index].userTempUnit = TempUnits.fahrenheit.rawValue
                default:
                    break;
                }
            }
            getParent().userDatabase.updateUser(userId: getParent().currentUser.userId!, userRole: "", userName: "", userCompany: "", userLanguage: "", userTempUnit: getParent().currentUser.userTempUnit!)
        }
        //Api user so just save changes for this logged in session
        else {
            switch tempControl.selectedSegmentIndex {
            case 0:
                getParent().userDatabase.currentUser.userTempUnit = TempUnits.celsius.rawValue
            case 1:
                getParent().userDatabase.currentUser.userTempUnit = TempUnits.fahrenheit.rawValue
            default:
                break;
            }
        }
    }

    //changes any labels and static UI stuff depending on user language preference
    func updateLabels() {
        if getParent().currentUser.userLanguage == Languages.english.rawValue {
            staticUserRealNameLabel.text = "Name:"
            staticUserNameLabel.text = "User Name:"
            staticUserNameLabel.font.withSize(17.0)
            staticUserCompanyLabel.text = "Company:"
            staticUserLanguageLabel.text =  "Language:"
            staticUserTempLabel.text = "Temperature Unit:"
            staticUserTempLabel.font.withSize(17.0)
            settingsLabel.text = "Settings"
        }
        else if getParent().currentUser.userLanguage == Languages.spanish.rawValue {
            staticUserRealNameLabel.text = "Nombre:"
            staticUserNameLabel.text = "Nombre de Usario:"
            staticUserNameLabel.adjustsFontSizeToFitWidth = true
            staticUserCompanyLabel.text = "Empresa:"
            staticUserLanguageLabel.text =  "Idioma:"
            staticUserTempLabel.text = "Unidad de Temperatura:"
            staticUserTempLabel.adjustsFontSizeToFitWidth = true
            settingsLabel.text = "Ajustes"
       
        }
    }
    
    func getParent() -> TabBarController
    {
        return (self.parent as! TabBarController)
    }
}
