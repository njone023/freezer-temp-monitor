//
//  SitesViewController.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 11/21/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import UIKit

class SitesViewController: UITableViewController, UISearchBarDelegate {
    
    var freezerOutOfRangeFound = false  //if at leaset one of the freezers for this user's sites have a bad temp
    var allUserSites: [UserSite]!       //contains all sites possible to display
    var userSites: [UserSite]!          //displays only sites that user searched, all if no search
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var backButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        self.searchBar.delegate = self
        
        //gets sites with same ID as current user
        allUserSites = getParent().getParent().allSites
        self.checkNavTitle()
        self.performSearch(keywords: "")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
        self.checkNavTitle()
        
        //display error alert if at least one of the user's freezers for any site have a temp out of tange
        for freezer in getParent().getParent().allFreezers {
            if ((freezer.freezerTempCapacity?.isLess(than: 0.45))! || !(freezer.freezerTempCapacity?.isLess(than: 0.55))!) && !freezerOutOfRangeFound {
                self.presentFreezerOutOfRangeAlert()
                freezerOutOfRangeFound = true
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.performSearch(keywords: searchText)
    }
    
    //search function, user can search by sitename or site address (which is not displayed)
    func performSearch(keywords: String?) {
        
        if let key = keywords {
            //trim keywords
            var searchKeywords = key.trimmingCharacters(in: .whitespacesAndNewlines)
            if searchKeywords.characters.count > 0 {
                //case insensitive search
                searchKeywords = searchKeywords.lowercased()
                
                var searchResults: [UserSite] = []
                //compare sites for match and if so, add to displayed sites array
                for site in allUserSites {
                    if(self.compare(keywords: searchKeywords, site: site)) {
                        searchResults.append(site)
                    }
                }
                self.userSites = searchResults
            }
            else {
                self.userSites = self.allUserSites
            }
        }
        tableView.reloadData()
    }
    
    //function to check if either the address or site name match keywords and if so return true
    func compare(keywords: String, site: UserSite) -> Bool {
        
        if site.siteAddress?.lowercased().range(of: keywords) != nil {
            return true
        }
        
        if site.siteName?.lowercased().range(of: keywords) != nil {
            return true
        }
        
        return false
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return userSites.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "SiteCell", for: indexPath) as! SiteCell
        
        cell.warningLabel.isHidden = true
        
        let site = userSites[indexPath.row]
        
        cell.siteNameLabel?.text = site.siteName
        
        for freezer in getParent().getParent().allFreezers {
            if site.siteId == freezer.siteId {
                if (freezer.freezerTempCapacity?.isLess(than: 0.45))! || !(freezer.freezerTempCapacity?.isLess(than: 0.55))! {
                    cell.warningLabel.isHidden = false
                    break
                }

            }
        }
        return cell
    }
    
    //sets the language of this view according to the user's settings
    func checkNavTitle() {
        if getParent().getParent().currentUser.userLanguage == Languages.english.rawValue{
            self.navigationItem.title = "Sites"
            searchBar.placeholder = "Search Sites by Name or Address"
            self.backButton.title = "Back"
        }
        else {
            self.navigationItem.title = "Sitios"
            searchBar.placeholder = "Buscar Sitios por Nombre o Dirección"
            self.backButton.title = "Atrás"
        }
    }
    
    //gest info from tab bar controller
    func getParent() -> NavigationController
    {
        return (self.parent as! NavigationController)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        let siteDetailViewController = segue.destination as? SiteDetailViewController
        
        if segue.identifier == "ShowSiteDetail" {
            
            let selectedItemCell = sender as? UITableViewCell
            let indexPath = tableView.indexPath(for: selectedItemCell!)
            let selectedItem = userSites[(indexPath?.row)!]
            var freezers: [SiteFreezer] = []
            for freezer in getParent().getParent().allFreezers {
                if selectedItem.siteId == freezer.siteId {
                    freezers.append(freezer)
                }
            }
            siteDetailViewController?.site = selectedItem
            siteDetailViewController?.user = getParent().getParent().currentUser
            siteDetailViewController?.freezers = freezers
            siteDetailViewController?.backButton = self.backButton
            siteDetailViewController?.isDatabseUser = getParent().getParent().isDatabaseUser
        }
    }
    
    //function to display error alert if freezer temp is out of range
    func presentFreezerOutOfRangeAlert() {
        let alert = UIAlertController(title: "Warning", message: "One or more of your freezers is out of temperature range. See site warnings.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
