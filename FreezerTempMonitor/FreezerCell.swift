//
//  FreezerCell.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/4/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import Foundation
import UIKit

class FreezerCell: UICollectionViewCell {
    
    @IBOutlet weak var freezerNameLabel: UILabel!
    @IBOutlet weak var freezerTempLabel: UILabel!
    @IBOutlet weak var freezerCapacityLabel: UILabel!
    

}
