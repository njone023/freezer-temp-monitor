//
//  SiteDetailViewController.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 11/21/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import UIKit

class SiteDetailViewController: UICollectionViewController {
    
    var site: UserSite!
    var user: User!
    var freezers: [SiteFreezer]!
    var tempUnitString = ""
    var backButton: UIBarButtonItem!
    var isDatabseUser = false
    
    override func viewDidLoad() {
        collectionView?.delegate = self
        collectionView?.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkTempUnit()                 //check user's temp unit setting
        checkLanguage()
        collectionView?.reloadData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return freezers.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FreezerCell", for: indexPath) as! FreezerCell
        
        let freezer = freezers[indexPath.row]
        
        let temp = getCorrectTemp(freezer: freezer)
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.locale = Locale(identifier: "en")
        
        cell.freezerNameLabel.text = freezer.freezerName
        cell.freezerTempLabel.text = String(describing: temp) + " " + tempUnitString
        cell.freezerCapacityLabel.text = formatter.string(for: freezer.freezerTempCapacity)
        
        return cell
    }
    
    //since core data is Int32, must get correct temperature out of a range of 86 values by subtracting 61 
    //from core data temp value (since 43-61 = -18 and -18ºC is the perfect temp for a freezer)
    //function also makes sure that correct temp is being displayed for user's temp unit setting
    func getCorrectTemp(freezer: SiteFreezer) -> Int {
        if isDatabseUser {
            if user.userTempUnit == TempUnits.celsius.rawValue {
                return Int(freezer.freezerTemp!) - 61   //default is celsius
            }
            else {
                return Int(Double((freezer.freezerTemp!) - 61) * (9.0/5.0) + 32)    //convert to fahrenheit
            }
        }
        else {
            if user.userTempUnit == TempUnits.celsius.rawValue {
                return Int(freezer.freezerTemp!)  //default is celsius
            }
            else {
                return Int(Double(freezer.freezerTemp!) * (9.0/5.0) + 32)    //convert to fahrenheit
            }
        }
        
    }
    
    func checkLanguage() {
        if user.userLanguage == Languages.english.rawValue {
            backButton.title = "Back"
        }
        else {
            backButton.title = "Atrás"
        }
    }
    
    //change temp unit label according to user's temp unit setting
    func checkTempUnit() {
        
        if user.userTempUnit == TempUnits.celsius.rawValue {
            tempUnitString = "Cº"
        }
        else {
            tempUnitString = "Fº"
        }
    }
}
