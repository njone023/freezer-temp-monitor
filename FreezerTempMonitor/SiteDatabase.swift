//
//  SiteDatabase.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/4/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class UserSite: NSObject {
    var userId: String?
    var siteName: String?
    var siteAddress: String?
    var siteId: String?
    
    init(userId: String?, siteName: String?, siteAddress: String?, siteId: String?)
    {
        self.userId = userId
        self.siteName = siteName
        self.siteAddress = siteAddress
        self.siteId = siteId
        
        super.init()
    }
}

class SiteDatabase {
    
    //var userSites: [UserSite]!          //we don't need to get site data until user loads table view
    var siteData: [NSManagedObject] = []
    
    internal init() {
        //self.addSite(userId: "testUser", siteName: "Site 5 Test", siteAddress: "Site 5 Test Address", siteId: "5")
        //self.addSite(userId: "testUser", siteName: "Site 2 Test", siteAddress: "Site 2 Test Address", siteId: "2")
        //self.addSite(userId: "testUser", siteName: "Site 3 Test", siteAddress: "Site 3 Test Address", siteId: "3")
    }
    
    func getUserSites(userId: String) -> [UserSite]? {
        
        var userSites: [UserSite] = []
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        
        let managedContext = appDelegate.persistentContainer2.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Site")
        do {
            siteData = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error)")
        }
        
        for site in siteData {
            var currSiteUserId: String = ""
            var currSiteName: String = ""
            var currSiteAddress: String = ""
            var currSiteId: String = ""

            for key in site.entity.propertiesByName.keys{
                let value: Any? = site.value(forKey: key)
                if key == "userId" {
                    currSiteUserId = value as! String
                }
                if key == "siteName" {
                    currSiteName = value as! String
                }
                if key == "siteAddress" {
                    currSiteAddress = value as! String
                }
                if key == "siteId" {
                    currSiteId = value as! String
                }
            }
            
            
            
            if currSiteUserId == userId {
                let currentSite = UserSite(userId: currSiteUserId, siteName: currSiteName, siteAddress: currSiteAddress, siteId: currSiteId)
                userSites.append(currentSite)
            }
        }
        print(userSites.count)
        return userSites
    }
    
    func addSite(userId: String, siteName: String, siteAddress: String, siteId: String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer2.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Site", in: managedContext)!
        
        let appUser = NSManagedObject(entity: entity, insertInto: managedContext)
        
        appUser.setValue(userId, forKey: "userId")
        appUser.setValue(siteName, forKey: "siteName")
        appUser.setValue(siteAddress, forKey: "siteAddress")
        appUser.setValue(siteId, forKey: "siteId")
        
        do {
            try managedContext.save()
            siteData.append(appUser)
        } catch let error as NSError {
            print("could not save \(error)")
        }
    }

}
