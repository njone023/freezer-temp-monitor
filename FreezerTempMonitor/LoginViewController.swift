//
//  ViewController.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 11/21/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    var userDatabase = UserDatabase()
    var apiManager = APIManager()
    var foundUser: Bool = false {
        didSet {
            checkUser()
        }
    }
    var sitesLoaded: Bool = false {
        didSet {
            checkUser()
        }
    }
    var isDatabaseUser: Bool = true
    
    @IBOutlet weak var loginButton: RoundedGradientButton!
    @IBOutlet var userNameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //user login function
    @IBAction func loginAction(_ sender: UIButton) {
        if (userNameTextField.text?.characters.count)! > 0 && (passwordTextField.text?.characters.count)! > 0 {
            
            //try to get user info from client API
            self.apiManager.getUser(userId: self.userNameTextField.text!, userPassword: self.passwordTextField.text!, completion: { isValidUser in
                if isValidUser.userId != "" && isValidUser.userName != "" {
                    self.userDatabase.currentUser = isValidUser
                    
                    self.isDatabaseUser = false
                    self.foundUser = true
                    
                    self.apiManager.getUserData(accessToken: isValidUser.accessToken!, userId: isValidUser.userId!, completion: { success in
                        DispatchQueue.main.async(execute: {
                            self.foundUser = true
                            self.sitesLoaded = success
                            return
                        })
                    })
            
                    return
                }
            })
   
            //if not API user, try to get from core data
            if let user = userDatabase.allUsers.first(where: { $0.userId == userNameTextField.text && $0.userPassword == passwordTextField.text}) {
                userDatabase.currentUser = user
                self.foundUser = true

                self.performSegue(withIdentifier: "GoToDashboard", sender: self)
                return
            }
            
            if let userDB = userDatabase.checkUserLogin(username: userNameTextField.text!, password: passwordTextField.text!) {
                userDatabase.currentUser = userDB
                self.foundUser = true
                
                self.performSegue(withIdentifier: "GoToDashboard", sender: self)
                return
            } 
        }
        self.checkUser()
    }
    
    //checks to see if the user is validated and go to dashboard, otherwise show error alert
    func checkUser() {
        if !foundUser {
            presentLoginErrorAlert()
            return
        }
        
        if sitesLoaded {
            self.performSegue(withIdentifier: "GoToDashboard", sender: self)
        }
    }
    
    //displays an alert if the user's credentials are incorrect
    func presentLoginErrorAlert() {
        let alert = UIAlertController(title: "Error", message: "User credentials not found or password is incorrect", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToDashboard"{
            let dashboardController = segue.destination as! TabBarController
            dashboardController.userDatabase = userDatabase
            dashboardController.isDatabaseUser = self.isDatabaseUser
            dashboardController.apiManager = self.apiManager
        } else if segue.identifier == "registerSegue"{
            let registerController = segue.destination as! RegisterViewController
            registerController.userDatabase = self.userDatabase
            registerController.apiManager = self.apiManager
        }
    }
    
    @IBAction func toggleText(_ sender: Any) {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
    }
    
    func alert(title: String, text: String) -> Void {
        // User exists
        let title = title
        let message = text
        let ac = UIAlertController(title: title,
                                   message: message,
                                   preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        ac.addAction(cancelAction)
        self.present(ac, animated: true, completion: nil)
    }
    
    @IBAction func forgotPassword(_ sender: Any) {
        alert(title: "Password forgot?", text: "You better create a new account, because password restore system will be avaulable in the future versions :)")
    }
    
}

