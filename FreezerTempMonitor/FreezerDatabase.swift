//
//  FreezerDatabase.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/4/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class SiteFreezer: NSObject {
    var freezerName: String?
    var freezerTemp: Int32?     //default temp will be in celsius and can be converted at any time
    var freezerTempCapacity: Double?
    var siteId: String?
    
    init(freezerName: String?, freezerTemp: Int32?, freezerTempCapacity: Double?, siteId: String?)
    {
        self.freezerName = freezerName
        self.freezerTemp = freezerTemp
        self.freezerTempCapacity = freezerTempCapacity
        self.siteId = siteId
        
        super.init()
    }
}

class FreezerDatabase {
   
    var freezerData: [NSManagedObject] = []
    
    internal init() {
        //self.addFreezer(freezerName: "Freezer 1", siteId: "5")
        //self.addFreezer(freezerName: "Freezer 2", siteId: "3")
        //self.addFreezer(freezerName: "Freezer 3", siteId: "3")
    }
    
    func getSiteFreezers(siteId: String) -> [SiteFreezer]? {
        
        var siteFreezers: [SiteFreezer] = []
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        
        let managedContext = appDelegate.persistentContainer3.viewContext
        
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Freezer")
        do {
            freezerData = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error)")
        }
        
        for freezer in freezerData {
            var currFreezerName: String = ""
            var currFreezerTemp: Int32!
            var currFreezerTempCapacity: Double!
            var currSiteId: String = ""
            
            for key in freezer.entity.propertiesByName.keys{
                let value: Any? = freezer.value(forKey: key)
                if key == "freezerName" {
                    currFreezerName = value as! String
                }
                if key == "freezerTemp" {
                    currFreezerTemp = value as! Int32
                }
                if key == "freezerTempCapacity" {
                    currFreezerTempCapacity = value as! Double
                }
                if key == "siteId" {
                    currSiteId = value as! String
                }
            }
            
            if currSiteId == siteId {
                let currentFreezer = SiteFreezer(freezerName: currFreezerName, freezerTemp: currFreezerTemp, freezerTempCapacity: currFreezerTempCapacity, siteId: currSiteId)
                siteFreezers.append(currentFreezer)
            }
        }
        print(siteFreezers.count)
        return siteFreezers
    }
    
    //add a freezer with random temp and calculated capacity
    func addFreezer(freezerName: String, siteId: String) {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let temp = arc4random_uniform(87) + 0     //actual temp will have to be determined when loaded because Int32 cannot be negative
        let capacity = Double(temp) / 86        //percent outof 100 where temp lies out of range of temperatures  (86 possible values)
        
        let managedContext = appDelegate.persistentContainer3.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "Freezer", in: managedContext)!
        
        let freezer = NSManagedObject(entity: entity, insertInto: managedContext)
        
        freezer.setValue(freezerName, forKey: "freezerName")
        freezer.setValue(temp, forKey: "freezerTemp")
        freezer.setValue(capacity, forKey: "freezerTempCapacity")
        freezer.setValue(siteId, forKey: "siteId")
        
        do {
            try managedContext.save()
            freezerData.append(freezer)
        } catch let error as NSError {
            print("could not save \(error)")
       }
    }

}
