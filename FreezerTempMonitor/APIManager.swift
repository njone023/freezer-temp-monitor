//
//  APIManager.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/3/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import Foundation
import UIKit

class APIManager {
    
    var userSites: [UserSite] = []
    var siteFreezers: [SiteFreezer] = []
    let urlString = "https://api.radily.com"
    var dataPrintable = false
    //var loggedInUser: User!
    
    init() {}
   
    /*** begin get user profile ***/
    //attempts to get user given user credentials
    func getUser(userId: String, userPassword: String, completion: @escaping (User) -> ()) {
        //var loggedInUser: User!
        //var loggedInUser = User(userId: "", userPassword: "", userRole: "", userName: "", userCompany: "",  userLanguage: "", userTempUnit: "")
        let urlPath = urlString + "/customers/users/authorize"
        guard let url = URL(string: urlPath) else {
            print("Error: Cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let body = ["userName": userId, "password": userPassword]
        let jsonBody: Data
        do {
            jsonBody = try JSONSerialization.data(withJSONObject: body, options: [])
            urlRequest.httpBody = jsonBody
            //let string = String(data: jsonBody, encoding: String.Encoding.utf8)
        } catch {
            print("Could not create json from data")
            return
        }
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: urlRequest) { (data, response, error) in
            //check for error
            guard error == nil else {
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                print(httpResponse.statusCode)
            }
            
            //make sure we got data
            guard let responseData = data else {
                print("Error: did not receieve data")
                return
            }
            
            //parse the results as JSON 
            do {
                
                //deserialize the json data into a dictionary
                guard let todo = try JSONSerialization.jsonObject(with: responseData, options: [])
                as? [String: Any] else {
                    print("Error trying to convert data to JSON")
                    return
                }
                
                //get general data as dictionary from request
                guard let first = todo["data"] as? [String: Any] else {
                    print("Could not get todo data from json")
                    return
                }
                
                //attempt to get user profile from data as dictionary
                guard let profile = first["profile"] as? [String: Any] else {
                    print("Could not get todo data from json")
                    return
                }
                
                guard let settings = profile["settings"] as? [String: Any] else {
                    print("Could not get todo data from json")
                    return
                }
                
                //get access token from user request
                guard let accessToken = first["access_token"] as? String else {
                    print("Could not get todo data from json")
                    return
                }
                
                //get user's name
                guard let userName = profile["name"] as? String else {
                    print("Could not get todo data from json")
                    return
                }
                
                //get user's role
                guard let userRole = profile["role"] as? String else {
                    print("Could not get todo data from json")
                    return
                }
                
                //get user's company
                guard let userCompany = profile["company"] as? String else {
                    print("Could not get todo data from json")
                    return
                }
                
                //get user's email
                guard let userEmail = profile["email"] as? String else {
                    print("Could not get todo data from json")
                    return
                }
                
                guard let userLanguage = settings["language"] as? String else {
                    print("Could not get todo data from json")
                    return
                }
                
                guard let userTempUnit = settings["temperatureUnit"] as? String else {
                    print("Could not get todo data from json")
                    return
                }
                //set current api access token to token receieved by get user request (if there is one)

            
                //print(userCompany)
                //print(accessToken)
                //print(userName)
               // print(userRole)
               // print(userEmail)
              //  print(userLanguage)
              //  print(userTempUnit)

                if self.dataPrintable {
                    print("userCompany: "+userCompany)
                    print("accessToken: "+accessToken)
                    print("userName: "+userName)
                    print("userRole: "+userRole)
                    print("userEmail: "+userEmail)
                    print("userLanguage: "+userLanguage)
                    print("userTempUnit: "+userTempUnit)
                }

                
                let loggedInUser = User(userId: userEmail, userPassword: "", userRole: userRole, userName: userName, userCompany: userCompany, userLanguage: userLanguage, userTempUnit: userTempUnit, accessToken: accessToken)
                
                completion(loggedInUser)
                
            } catch {
                print("error")
                //return
            }
        }
        task.resume()
      
        //return loggedInUser
    }
    /**** end user profile ****/
    
    /***Begin get user sites and devices ***/
    func getUserData(accessToken: String, userId: String, completion: @escaping (Bool) -> ()){
        let urlPath = urlString + "/customers/devices"
        guard let url = URL(string: urlPath) else {
            print("Error: Cannot create URL")
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        let task = session.dataTask(with: urlRequest) { (data, response, error) in
            //check for error
            guard error == nil else {
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                print(httpResponse.statusCode)
                if !(httpResponse.statusCode >= 200 && httpResponse.statusCode < 299) {
                    completion(false)
                }
                else {
                    //make sure we got data
                    guard let responseData = data else {
                        print("Error: did not receieve data")
                        return
                    }
                    
                    //parse the results as JSON
                    do {
                        
                        //deserialize the json data into a dictionary
                        guard let rawData = try JSONSerialization.jsonObject(with: responseData, options: [])
                            as? [String: Any] else {
                                print("Error trying to convert data to JSON")
                                return
                        }
                        
                        //get general data as dictionary from request
                        guard let data = rawData["data"] as? [Any] else {
                            print("Could not get todo data from json")
                            return
                        }
                        
                        for site in data {
                            guard let aSite = site as? [String: Any] else {
                                return
                            }
                            let siteName = aSite["name"]
                            let siteId = aSite["id"]
                            guard let devices = aSite["devices"] as? [Any] else {
                                return
                            }
                            for device in devices {
                                guard let aDevice = device as? [String: Any] else {
                                    return
                                }
                                let deviceSiteId = aDevice["siteId"]
                                let deviceTemp = aDevice["temperature"] as? Int32
                                let name = aDevice["name"]
                                let currFreezer = SiteFreezer(freezerName: name as? String, freezerTemp: deviceTemp, freezerTempCapacity: 0, siteId: deviceSiteId as? String)
                                self.siteFreezers.append(currFreezer)
                            }
                            let currSite = UserSite(userId: userId, siteName: siteName as? String, siteAddress: "", siteId: siteId as? String)
                            self.userSites.append(currSite)
                        }
                        
                        
                        
                    } catch {
                        print("error")
                        //return
                    }
                    completion(true)
                }
            }
        }
        task.resume()

    }
}
