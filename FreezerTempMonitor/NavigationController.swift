//
//  NavigationController.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/4/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import Foundation
import UIKit

class NavigationController: UINavigationController {
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    func getParent() -> TabBarController
    {
        return (self.parent as! TabBarController)
    }
}
