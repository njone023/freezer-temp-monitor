//
//  UserDatabase.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/3/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import UIKit
import Foundation
import CoreData

enum Roles: String {
    case admin = "administrator"
    case user =  "user"
}

enum Languages: String {
    case english = "en"
    case spanish = "es"
}

enum TempUnits: String {
    case celsius = "Celsius"
    case fahrenheit = "Fahrenheit"
}

//represents AppUser entity
class User: NSObject {
    var userId: String?
    var userPassword: String?
    var userRole: String?
    var userName: String?
    var userCompany: String?
    var userLanguage: String?
    var userTempUnit: String?
    var accessToken: String?
    
    init(userId: String?, userPassword: String?, userRole: String?, userName: String?, userCompany: String?, userLanguage: String?, userTempUnit: String?, accessToken: String?)
    {
        self.userId = userId
        self.userPassword = userPassword
        self.userRole = userRole
        self.userName = userName
        self.userCompany = userCompany
        self.userLanguage = userLanguage
        self.userTempUnit = userTempUnit
        self.accessToken = accessToken //if user is on API
        
        super.init()
    }
}

class UserDatabase {
    var currentUser: User!
    var allUsers = [User]()                 //create user objects from user core data
    var userData: [NSManagedObject] = []    //user data pulled from core data
    
    internal init()
    {
        //need to append here with my log in information after i log in as admin with my credentials
        
        //self.registerUser(userId: "testUser", userPassword: "1234", userRole: Roles.user.rawValue, userName: "Test User", userCompany: "Test Company", userLanguage: //Languages.english.rawValue, userTempUnit: TempUnits.fahrenheit.rawValue)
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "AppUser")
        do {
            userData = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error)")
        }
        
        //iterate through data and add each to users array
        for user in userData {
            var currUserId: String = ""
            var currUserPwd: String = ""
            var currUserRole: String = ""
            var currUserName: String = ""
            var currUserCompany: String = ""
            var currUserLanguage: String = ""
            var currUserTempUnit: String = ""
            for key in user.entity.propertiesByName.keys{
                let value: Any? = user.value(forKey: key)
                if key == "userId" {
                    currUserId = value as! String
                }
                if key == "userPassword" {
                    currUserPwd = value as! String
                }
                if key == "userRole" {
                    if value != nil {
                        currUserRole = value as! String
                    }
                }
                if key == "userName" {
                    currUserName = value as! String
                }
                if key == "userCompany" {
                    currUserCompany = value as! String
                }
                if key == "userLanguage" {
                    currUserLanguage = value as! String
                }
                if key == "userTempUnit" {
                    currUserTempUnit = value as! String
                }
                
            }
            let currentUser = User(userId: currUserId, userPassword: currUserPwd, userRole: currUserRole, userName: currUserName, userCompany: currUserCompany, userLanguage: currUserLanguage, userTempUnit: currUserTempUnit, accessToken: "")
            allUsers.append(currentUser)
        }
//        print(allUsers.count)
        
    }
    
    //function that gets called after someone successfully registers as a user of the app (to save them to core data)
    func registerUser(userId: String, userPassword: String, userRole: String = "user", userName: String, userCompany: String, userLanguage: String = "en", userTempUnit: String = "Celsius") -> Bool {
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return false
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let entity = NSEntityDescription.entity(forEntityName: "AppUser", in: managedContext)!
        
        let appUser = NSManagedObject(entity: entity, insertInto: managedContext)
        
        appUser.setValue(userId, forKey: "userId")
        appUser.setValue(userPassword, forKey: "userPassword")
        appUser.setValue(userRole, forKey: "userRole")
        appUser.setValue(userName, forKey: "userName")
        appUser.setValue(userCompany, forKey: "userCompany")
        appUser.setValue(userLanguage, forKey: "userLanguage")
        appUser.setValue(userTempUnit, forKey: "userTempUnit")
        
        do {
            try managedContext.save()
            userData.append(appUser)
            return true
        } catch let error as NSError {
            print("could not save \(error)")
        }
        
        return false
    }
    
    //make changes to a user instead of adding a new one
    func updateUser(userId: String, userRole: String, userName: String, userCompany: String, userLanguage: String, userTempUnit: String)
    {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "AppUser")
        let predicate = NSPredicate(format: "userId = '\(userId)'")
        fetchRequest.predicate = predicate
        do {
            let test = try managedContext.fetch(fetchRequest)
            if test.count == 1 {
                let objectUpdate = test[0] as! NSManagedObject
                if userRole != "" {
                    objectUpdate.setValue(userRole, forKey: "userRole")
                }
                if userName != "" {
                    objectUpdate.setValue(userName, forKey: "userName")
                }
                
                if userLanguage != "" {
                    objectUpdate.setValue(userLanguage, forKey: "userLanguage")
                }
                if userTempUnit != "" {
                    objectUpdate.setValue(userTempUnit, forKey: "userTempUnit")
                }
            }
        }
        catch let error as NSError{
            print(error)
        }
        do {
            try managedContext.save()
        }
        catch let error as NSError {
            print(error)
        }
    }
    
    func checkUserExistance(username: String) -> Bool? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "AppUser")
        let predicate = NSPredicate(format: "userName = '\(username)'")
        fetchRequest.predicate = predicate
        do {
            let test = try managedContext.fetch(fetchRequest)
            if test.count == 0 {
                return true
            } else {
                return false
            }
            
        } catch {
            print("Could not load data \(error.localizedDescription)")
        }
        return false
    }
    
    func checkUserLogin(username: String, password: String) -> User? {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return nil
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "AppUser")
        let predicate = NSPredicate(format: "userId = '\(username)'")
        fetchRequest.predicate = predicate
        do {
            let test = try managedContext.fetch(fetchRequest)
            
            for te in test {
                print("Input: "+password+", DBPass: "+(te as AnyObject).userPassword!! as! String)
                if (te as AnyObject).userPassword as! String == password {
                    let user = User(userId: (te as AnyObject).userId as! String, userPassword: (te as AnyObject).userPassword as! String, userRole: (te as AnyObject).userRole as! String, userName: (te as AnyObject).userName as! String, userCompany: (te as AnyObject).userCompany as! String, userLanguage: (te as AnyObject).userLanguage as! String, userTempUnit: (te as AnyObject).userTempUnit as! String, accessToken: "")
                    return user
                } else {
                    print("passwords don't match")
                    return nil
                }
            }
            
        } catch {
            print("Could not load data \(error.localizedDescription)")
        }
        return nil
    }

    
    /*
    @discardableResult func createUser(empId: String?, empName: String?, empRole: String) -> User
    {
        let newUser = User(id: empId, name: empName, role: empRole)
        allUsers.append(newUser)
        
        return newUser
    } 
     */
    
    /*
    func deleteUser(_ user: User) {
        if let index = allUsers.index(of: user) {
            allUsers.remove(at: index)
        }
    }
 */
}
