//
//  TabBarController.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/3/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import Foundation
import UIKit

class TabBarController: UITabBarController {
    
    var apiManager: APIManager!
    var userDatabase: UserDatabase!
    var currentUser: User!
    var isDatabaseUser = false
    var allSites: [UserSite]!
    var sitesDatabase =  SiteDatabase()
    var allFreezers: [SiteFreezer] = []         //array of ALL freezers in each site
    var freezerDatabase = FreezerDatabase()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let user = userDatabase.currentUser {
            self.currentUser = user
        }
        
        if !isDatabaseUser {
            allSites = apiManager.userSites
            allFreezers = apiManager.siteFreezers
        }
        else {
            allSites = sitesDatabase.getUserSites(userId: currentUser.userId!)
            for site in allSites {
                let freezers = freezerDatabase.getSiteFreezers(siteId: site.siteId!)
                for freezer in freezers! {
                    allFreezers.append(freezer)
                }
            }

        }
        self.checkLabels()
    }
    
    func checkLabels() {
        if currentUser.userLanguage == Languages.english.rawValue {
            tabBar.items?[0].title = "Dashboard"
            tabBar.items?[1].title = "Profile"
        }
        else {
            tabBar.items?[0].title = "Tablero"
            tabBar.items?[1].title = "Perfil"
        }
    }
    
    
    
}
