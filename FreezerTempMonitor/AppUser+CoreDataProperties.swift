//
//  AppUser+CoreDataProperties.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/3/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import Foundation
import CoreData

extension AppUser {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<AppUser> {
        return NSFetchRequest<AppUser>(entityName: "AppUser")
    }

    @NSManaged public var userId: String?
    @NSManaged public var userPassword: String?
    @NSManaged public var userRole: String?
    @NSManaged public var userFirstName: String?
    @NSManaged public var userLastName: String?
    @NSManaged public var userCompany: String?
    @NSManaged public var userLanguage: String?
    @NSManaged public var userTempUnit: String?

}
