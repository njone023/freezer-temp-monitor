//
//  RoundedGradientButton.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 11/21/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import UIKit

@IBDesignable 
class RoundedGradientButton: UIButton {
    let gradientLayer = CAGradientLayer()

    @IBInspectable
    var roundedRadius: CGFloat = 10 {
        didSet {
            super.layer.cornerRadius = roundedRadius
            self.setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    @IBInspectable
    var topGradientColor: UIColor = .init(colorLiteralRed: 121/255.0, green: 198/255.0, blue: 1, alpha: 1){
        didSet {
            self.setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    @IBInspectable
    var bottomGradientColor: UIColor = .init(colorLiteralRed: 0, green: 122/255.0, blue: 1, alpha: 1) {
        didSet {
           self.setGradient(topGradientColor: topGradientColor, bottomGradientColor: bottomGradientColor)
        }
    }
    
    private func setGradient(topGradientColor: UIColor?, bottomGradientColor: UIColor?) {
        if let topGradientColor = topGradientColor, let bottomGradientColor = bottomGradientColor {
            self.gradientLayer.frame = super.bounds
            self.gradientLayer.colors = [topGradientColor.cgColor, bottomGradientColor.cgColor]
            self.gradientLayer.borderColor = super.layer.borderColor
            self.gradientLayer.borderWidth = super.layer.borderWidth
            self.gradientLayer.cornerRadius = super.layer.cornerRadius
            super.layer.insertSublayer(self.gradientLayer, at: 0)
        } else {
            self.gradientLayer.removeFromSuperlayer()
        }
    }
}
