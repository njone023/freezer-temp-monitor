//
//  RegisterViewController.swift
//  FreezerTempMonitor
//
//  Created by Mykyta Smyrnov on 12/6/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {
    
    // MARK: Variables
    @IBOutlet weak var usernameField: UITextField!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var companyField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var passwordFieldRepeat: UITextField!
    
    var userDatabase: UserDatabase!
    var apiManager: APIManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reset()
    }
    
    @IBAction func dismissView(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: Actions
    //
    // Register button pressed - and now the game begins :)
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        print("Register pressed")
        // check if all fields are filled
        guard let username = usernameField.text, username.characters.count > 3 && username.characters.count <= 16  else {
            alert(text: "Wrong typing of username")
            return
        }
        guard let name = nameField.text, name.characters.count > 3 && name.characters.count <= 16  else {
            alert(text: "Wrong typing of name")
            return
        }
        guard let company = companyField.text, company.characters.count > 3 && company.characters.count <= 16  else {
            alert(text: "Wrong typing of company")
            return
        }
        guard let password = passwordField.text, password.characters.count > 3 && password.characters.count <= 16  else {
            alert(text: "Wrong typing of password")
            return
        }
        guard let passwordRepeat = passwordFieldRepeat.text, passwordRepeat.characters.count > 3 && passwordRepeat.characters.count <= 16  else {
            alert(text: "Wrong typing of password repeat field")
            return
        }
        // check if both passwords are the same
        if passwordField.text != passwordFieldRepeat.text {
            alert(text: "The two input passwords are different")
            // clean both passwords
            passwordField.text = ""
            passwordFieldRepeat.text = ""
            return
        }
        
        // call a connection fiunction
        self.requestRegister(username: username, name: name, company: company, password: password)
    }
    
    @discardableResult func requestRegister(username: String, name: String, company: String, password: String) -> Bool {
        
        if let call1 = self.userDatabase.checkUserExistance(username: username), call1 == true {
            // send a request to the server
            let callback = self.userDatabase.registerUser(userId: username, userPassword: password, userName: name, userCompany: company)
            if callback { 
                // dismiss segue
                dismiss(animated: true, completion: nil)
            }
        } else {
            alert(text: "Username does exist!")
            // empty user name
            usernameField.text = ""
            return false
        }
        return true
    }
    
    // MARK: Auxillary functions
    //
    // Reset all the fields to empty
    func reset(){
        usernameField.text = ""
        nameField.text = ""
        companyField.text = ""
        passwordField.text = ""
        passwordFieldRepeat.text = ""
    }
    // Show an alert
    func alert(text: String) -> Void {
        // User exists
        let title = text
        let message = text
        let ac = UIAlertController(title: title,
                                   message: message,
                                   preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        ac.addAction(cancelAction)
        self.present(ac, animated: true, completion: nil)
    }
    // toggle secure password entry
    @IBAction func toggleText(_ sender: Any) {
        passwordField.isSecureTextEntry = !passwordField.isSecureTextEntry
        passwordFieldRepeat.isSecureTextEntry = !passwordFieldRepeat.isSecureTextEntry
    }
    
}
