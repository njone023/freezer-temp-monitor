//
//  Site+CoreDataProperties.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/4/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import Foundation
import CoreData


extension Site {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Site> {
        return NSFetchRequest<Site>(entityName: "Site")
    }

    @NSManaged public var siteId: String?
    @NSManaged public var siteName: String?
    @NSManaged public var siteAddress: String?
    @NSManaged public var userId: String?

}
