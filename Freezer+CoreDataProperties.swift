//
//  Freezer+CoreDataProperties.swift
//  FreezerTempMonitor
//
//  Created by Nicole Jones on 12/4/18.
//  Copyright © 2018 Florida International University. All rights reserved.
//

import Foundation
import CoreData


extension Freezer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Freezer> {
        return NSFetchRequest<Freezer>(entityName: "Freezer")
    }

    @NSManaged public var freezerName: String?
    @NSManaged public var freezerTemp: Int32
    @NSManaged public var freezerTempCapacity: Int32
    @NSManaged public var siteId: String?

}
